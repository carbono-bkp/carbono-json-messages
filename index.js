'use strict';
/**
 * carbono-json-messages module.
 * This module must be used by all carbono platform
 * to generate json messages over APIs.
 *
 * @author Carbono Team
 * @module carbono-json-messages
 */
var CarbonoJsonMessages = require('./lib/json-messages');

module.exports = CarbonoJsonMessages;
